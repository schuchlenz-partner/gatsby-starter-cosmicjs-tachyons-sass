import React, { Component } from 'react';
import { Helmet } from 'react-helmet'
import Layout from '../components/Layout/Layout'

class Index extends Component {
  /* constructor(props) {
    super(props);
    this.state = {  }
  } */
  
  render() { 
    return (
      <Layout extraClasses="pa3">
        <Helmet>
          <title>Gatsby Tachyons CosmicJS SASS Starter</title>
        </Helmet>
        <div className="tc">
          <h1 className="f1 lh-headline fw9 tracked-tight mb0 pb0">Gatsby Tachyons CosmicJS SASS Starter</h1>
          <h2 className="f4 ma0 fw2">
            by
            &nbsp; 
            <a href="https://gitlab.com/schuchlenz-partner/" className="near-black dim black-hover no-underline fw7" target="_BLANK" rel="noopener noreferrer">
              <span className="pr1"><span className="iconify" data-icon="logos:gitlab" title="Schuchlenz &amp; Partner on Gitlab">&nbsp;</span></span>
              Schuchlenz &amp; Partner
            </a>
          </h2>
        </div>
      </Layout>
    );
  }
}
 
export default Index;