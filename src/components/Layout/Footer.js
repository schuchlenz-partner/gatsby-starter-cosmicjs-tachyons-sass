import React from 'react';

const Footer = () => {
  return (
    <div className="tc f7 pv3" style={{ position: 'fixed', bottom: 0, left: 0, width: '100%'}}>
      &copy; Schuchlenz &amp; Partner 2020. 
      Powered by &nbsp;
      <span className="pr1"><span className="iconify" data-icon="logos:gatsby"></span></span>GatsbyJS,
      <span className="pr1"><span className="iconify" data-icon="logos:graphql"></span></span>graphQL &amp;&nbsp;
      <span className="pr1"><span className="iconify" data-icon="logos:nodejs-icon"></span></span>nodeJS
    </div>
  );
}
 
export default Footer;