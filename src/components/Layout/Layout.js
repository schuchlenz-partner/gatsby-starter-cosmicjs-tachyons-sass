import React from 'react';
import Header from './Header'
import Footer from './Footer'
import { Helmet } from 'react-helmet'

import 'simple-line-icons/css/simple-line-icons.css'
import 'tachyons/css/tachyons.css'
import './Layout.sass'

const Layout = (props) => {
  return (
    <div className={"pa0 sans-serif " + props.extraClasses }>
      <Helmet>
        <html lang="de"></html>
        <script src="https://code.iconify.design/1/1.0.3/iconify.min.js"></script>
      </Helmet>

      <Header/>
      {props.children}
      <Footer/>
    </div>
  );
}
 
export default Layout;